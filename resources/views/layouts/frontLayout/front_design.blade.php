<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title>Eshop - eCommerce HTML5 Template.</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{ asset('images/frontend_images/favicon.png') }}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('css/frontend_css/bootstrap.css') }}">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/magnific-popup.min.css') }}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/font-awesome.css') }}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{ asset('css/frontend_css/jquery.fancybox.min.css') }}">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/themify-icons.css') }}">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/niceselect.css') }}">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/animate.css') }}">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/flex-slider.min.css') }}">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/owl-carousel.css') }}">
	<!-- Slicknav -->
    <link rel="stylesheet" href="{{ asset('css/frontend_css/slicknav.min.css') }}">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{ asset('css/frontend_css/reset.css') }}">
	<link rel="stylesheet" href="{{ asset('css/frontend_css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/frontend_css/responsive.css') }}">

	<!-- Template StyleSheet -->

	<link href="{{ asset('css/frontend_css/template/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/template/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/template/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/template/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/template/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('css/frontend_css/template/main.css') }}" rel="stylesheet">
	<link href="{{ asset('css/frontend_css/template/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('css/frontend_css/template/easyzoom.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ asset('js/frontend_css/template/html5shiv.js') }}"></script>
    <script src="{{ asset('js/frontend_css/template/respond.min.js') }}"></script>
    <![endif]-->  
	
</head>
<body class="js">
	
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
		
	
    @include('layouts.frontLayout.front_header')

    @yield('content')
    	
	@include('layouts.frontLayout.front_footer')

	<!-- Jquery -->
    <script src="{{ asset('js/frontend_js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/frontend_js/jquery-migrate-3.0.0.js') }}"></script>
	<script src="{{ asset('js/frontend_js/jquery-ui.min.js') }}"></script>
	<!-- Popper JS -->
	<script src="{{ asset('js/frontend_js/popper.min.js') }}"></script>
	<!-- Bootstrap JS -->
	<script src="{{ asset('js/frontend_js/bootstrap.min.js') }}"></script>
	<!-- Color JS -->
	<script src="{{ asset('js/frontend_js/colors.js') }}"></script>
	<!-- Slicknav JS -->
	<script src="{{ asset('js/frontend_js/slicknav.min.js') }}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{ asset('js/frontend_js/owl-carousel.js') }}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{ asset('js/frontend_js/magnific-popup.js') }}"></script>
	<!-- Waypoints JS -->
	<script src="{{ asset('js/frontend_js/waypoints.min.js') }}"></script>
	<!-- Countdown JS -->
	<script src="{{ asset('js/frontend_js/finalcountdown.min.js') }}"></script>
	<!-- Nice Select JS -->
	<script src="{{ asset('js/frontend_js/nicesellect.js') }}"></script>
	<!-- Flex Slider JS -->
	<script src="{{ asset('js/frontend_js/flex-slider.js') }}"></script>
	<!-- ScrollUp JS -->
	<script src="{{ asset('js/frontend_js/scrollup.js') }}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{ asset('js/frontend_js/onepage-nav.min.js') }}"></script>
	<!-- Easing JS -->
	<script src="{{ asset('js/frontend_js/easing.js') }}"></script>
	<!-- Active JS -->
	<script src="{{ asset('js/frontend_js/active.js') }}"></script>

{{-- template --}}
	<script src="{{ asset('js/frontend_js/template/jquery.js') }}"></script>
	<script src="{{ asset('js/frontend_js/template/price-range.js') }}"></script>
    <script src="{{ asset('js/frontend_js/template/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('js/frontend_js/template/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/frontend_js/template/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('js/frontend_js/template/main.js') }}"></script>
    <script src="{{ asset('js/frontend_js/template/easyzoom.js') }}"></script>
</body>
</html>