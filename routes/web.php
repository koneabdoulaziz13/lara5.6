<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */


/* Route::get('/', function () {
    return view('coming-soon');
}); */
//Index
Route::get('/', 'IndexController@index');
//Users register page	
Route::get('/login-register','usersController@userLoginRegister');
//Users register submit
Route::post('/user-register','usersController@register');
Route::post('/user-logout','usersController@logout');



//Product details
Route::get('/product/{id}', 'ProductsController@product');
// Get Product Attribute Price
Route::any('/get-product-price','ProductsController@getProductPrice');

//Add to cart
Route::match(['get','post'], '/add-cart', 'ProductsController@addtocart');
Route::match(['get','post'], '/cart', 'ProductsController@cart');
//listing Cat
Route::get('/products/{url}', 'ProductsController@products');
//delete product in Cart
Route::get('/cart/delete-product/{id}', 'ProductsController@deleteCartProduct');
//Update product quantity in Cart
Route::get('/cart/update-quantity/{id}/{quantity}', 'ProductsController@updateCartQuantity');



Route::group(['middleware' => ['auth']],function(){
    Route::get('/admin/dashboard','AdminController@dashboard');	
    Route::get('/admin/settings','AdminController@settings');	
    Route::get('/admin/check-pwd','AdminController@chkPassword');
    Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');

    Route::match(['GET', 'POST'],'/admin/add-category','CategoryController@AddCategory');
    Route::match(['GET', 'POST'],'/admin/edit-category/{id}','CategoryController@editCategory');
    Route::match(['GET', 'POST'],'/admin/delete-category/{id}','CategoryController@deleteCategory');
    Route::get('/admin/view-categories', 'CategoryController@viewCategories');

    Route::match(['GET', 'POST'],'/admin/add-product','ProductsController@AddProduct');
    Route::get('/admin/view-products', 'ProductsController@ViewProducts');
    Route::get('/admin/edit-product/{id}','ProductsController@editProduct');
    Route::get('/admin/delete-product-image/{id}','ProductsController@deleteProductImage');
    Route::get('/admin/delete-product/{id}','ProductsController@deleteProduct');
    Route::get('/admin/delete-alt-image/{id}','ProductsController@deleteAltImage');

    //Product managment
    Route::match(['GET', 'POST'],'/admin/add-attributes/{id}','ProductsController@AddAttributes');
    Route::match(['GET', 'POST'],'/admin/edit-attributes/{id}','ProductsController@editAttributes');
    Route::get('/admin/delete-attribute/{id}','ProductsController@deleteAttribute');
    Route::match(['get', 'post'], '/admin/add-images/{id}','ProductsController@addImages');



});
Route::match(['get','post'], '/admin', 'AdminController@login');
//Route::get('/admin', 'AdminController@login');
Route::get('/logout','AdminController@logout');	

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
