<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class usersController extends Controller
{
    //
    
    public function userLoginRegister(){
       
        return view('users.login_register');
    }
    public function register(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $usersCount = User::where('email', $data['email'])->count();
            //echo "<pre>"; print_r($data); die;
              //return redirect('/admin/settings')->with('flash_message_success','Password updated Successfully!');
            if ($usersCount>0) {
            
                return redirect()->back()->with('flash_message_error','Email existe dejà');
            }else {
                $user = new User;
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->save();
                if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin'=>'1'])) {
            
                    return redirect('/');
                    //return redirect('/cart');
                }

                //echo "success"; die;
            }
        }
        //return view('users.login_register');
    }
    public function logout(){
       
        Auth::logout();
        return view('/');
    }
}
