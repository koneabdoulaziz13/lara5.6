<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Banner;

class IndexController extends Controller
{
    
    public function index()
    {
        // Get all Products
    	$productsAll = Product::get();
    	$productsAll = Product::orderBy('id', 'DESC')->get();
    	$productsAll = Product::inRandomOrder()->get();
    	$categories = Category::where(['parent_id' => 0])->get();
    	$productsAll = Product::inRandomOrder()->where('status',1)->get();
    	//$productsAll = json_decode(json_encode($productsAll));
    	/*echo "<pre>"; print_r($productsAll);die;*/

		//For sub cat in blade no relationship (querybuilder)
        /* $categories_menu = "";
        foreach ($categories as $cat) {
            //echo $cat->name; echo "</br>";
            $categories_menu .= "<section class='accordion-item'>
				<h4><a href='#'".$cat->id." >".$cat->name."</a></li>
				<div class='accordion-item-content'> <ul class='categor-list'>";
			
			$sub_cat = Category::where(['parent_id' => $cat->id])->get();
            foreach ($sub_cat as $sc) {
                //echo $sc->name; echo "</br>";
				$categories_menu .=  "
				<li><a href=".$sc->url.">".$sc->name."</a></li>";
            }
			$categories_menu .= "</ul> </div> </section> "; 
        } */
        //die;
    	// Get All Categories and Sub Categories
    	/* $categories_menu = "";
    	$categories = Category::with('categories')->where(['parent_id' => 0])->get();
    	$categories = json_decode(json_encode($categories)); */
    	/*echo "<pre>"; print_r($categories); die;*/
		/* foreach($categories as $cat){
			$categories_menu .= "
			<div class='panel-heading'>
				<h4 class='panel-title'>
					<a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
						<span class='badge pull-right'><i class='fa fa-plus'></i></span>
						".$cat->name."
					</a>
				</h4>
			</div>
			<div id='".$cat->id."' class='panel-collapse collapse'>
				<div class='panel-body'>
					<ul>";
					$sub_categories = Category::where(['parent_id' => $cat->id])->get();
					foreach($sub_categories as $sub_cat){
						$categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
					}
						$categories_menu .= "</ul>
				</div>
			</div>
			";		
		}

		$banners = Banner::where('status','1')->get(); */

    	return view('index')->with(compact('productsAll', 'categories'));
    	//return view('index')->with(compact('productsAll', 'categories_menu', 'categories'));
    	//return view('index')->with(compact('productsAll','categories_menu','categories','banners'));
    }
}
